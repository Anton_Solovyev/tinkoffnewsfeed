package com.asolovyev.newsfeed.network.response;

@SuppressWarnings("unused")
public abstract class BaseResponse<T> {
    private String resultCode;
    private T payload;

    public String getResultCode() {
        return resultCode;
    }

    public T getPayload() {
        return payload;
    }
}
