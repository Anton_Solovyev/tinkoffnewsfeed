package com.asolovyev.newsfeed.network;

import com.asolovyev.newsfeed.network.response.NewsFeedContentResponse;
import com.asolovyev.newsfeed.network.response.NewsFeedResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("v1/news")
    Call<NewsFeedResponse> getNews();

    @GET("v1/news_content")
    Call<NewsFeedContentResponse> getNewsContent(@Query("id") String serverID);
}
