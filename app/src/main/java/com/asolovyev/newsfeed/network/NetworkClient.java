package com.asolovyev.newsfeed.network;

import com.asolovyev.newsfeed.model.NewsFeed;
import com.asolovyev.newsfeed.network.deserializer.NewsFeedDeserializer;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {
    private static final String BASE_URL = "https://api.tinkoff.ru/";

    private final Api api;

    public NetworkClient() {
        final Retrofit.Builder retrofitBuilder = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(
                        GsonConverterFactory
                                .create(new GsonBuilder().registerTypeAdapter(NewsFeed.class,
                                        new NewsFeedDeserializer()).create()));

        api = retrofitBuilder.build().create(Api.class);
    }

    public Api getApi() {
        return api;
    }
}
