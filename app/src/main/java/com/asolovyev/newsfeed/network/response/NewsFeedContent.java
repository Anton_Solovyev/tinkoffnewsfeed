package com.asolovyev.newsfeed.network.response;

public class NewsFeedContent {
    private String content;

    public NewsFeedContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
