package com.asolovyev.newsfeed.feature.content;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Spanned;
import android.view.View;
import android.widget.TextView;

import com.asolovyev.newsfeed.R;
import com.asolovyev.newsfeed.model.NewsFeed;

public class NewsFeedContentActivity extends AppCompatActivity implements Contract.View{

    //================================================================================
    // Launch intent
    //================================================================================

    public static Intent createIntent(@NonNull Context context, @NonNull NewsFeed newsFeed) {
        final Intent intent = new Intent(context, NewsFeedContentActivity.class);
        intent.putExtra(Contract.ARG_NEWS_FEED, newsFeed);
        return intent;
    }

    //================================================================================
    // Fields
    //================================================================================

    private TextView dateView;
    private TextView textView;
    private TextView contentView;
    private View progressBarContainer;
    private TextView errorView;

    private NewsFeedContentPresenter presenter;

    //================================================================================
    // AppCompatActivity methods override
    //================================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsfeed_content);

        dateView = (TextView) findViewById(R.id.dateView);
        textView = (TextView) findViewById(R.id.textView);
        contentView = (TextView) findViewById(R.id.contentView);
        progressBarContainer = findViewById(R.id.progressBarContainer);
        errorView = (TextView) findViewById(R.id.errorView);

        presenter = new NewsFeedContentPresenter();
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        presenter.detachView();

        super.onStop();
    }

    //================================================================================
    // Contract.View implementation
    //================================================================================

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public Bundle getExtras() {
        return getIntent().getExtras();
    }


    @Override
    public void setContentText(Spanned contentText) {
        contentView.setText(contentText);
    }

    @Override
    public void setDate(String date) {
        dateView.setText(date);
    }

    @Override
    public void setErrorVisible(boolean isVisible) {
        errorView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setProgressBarVisible(boolean isVisible) {
        progressBarContainer.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setTitle(Spanned title) {
        textView.setText(title);
    }
}
