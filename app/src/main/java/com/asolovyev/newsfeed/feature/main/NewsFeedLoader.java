package com.asolovyev.newsfeed.feature.main;

import android.content.Context;
import android.support.annotation.NonNull;

import com.asolovyev.newsfeed.db.Database;
import com.asolovyev.newsfeed.db.TablesConfig;
import com.asolovyev.newsfeed.model.NewsFeed;
import com.asolovyev.newsfeed.shared.loader.BaseLoader;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

class NewsFeedLoader extends BaseLoader<List<NewsFeed>> {
    //================================================================================
    // Fields
    //================================================================================

    private final Database database;
    private long lastPublicationDate;

    NewsFeedLoader(@NonNull Context context, @NonNull Database database) {
        super(context);
        this.database = database;
    }

    //================================================================================
    // BaseLoader methods override
    //================================================================================

    @Override
    public List<NewsFeed> loadInBackground() {
        Dao<NewsFeed, Long> dao = null;
        try {
            dao = database.getNewsFeedDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<NewsFeed> resultList = null;
        if(dao == null) {
            resultList = Collections.emptyList();
        } else {
            try {
                final QueryBuilder<NewsFeed, Long> builder = dao.queryBuilder()
                        .limit((long) Config.ITEMS_PER_PAGE)
                        .orderBy(TablesConfig.NewsFeed.PUBLICATION_DATE, false);
                if(lastPublicationDate == 0) {
                    resultList = builder.query();
                } else {
                    resultList = builder
                            .where()
                            .lt(TablesConfig.NewsFeed.PUBLICATION_DATE, lastPublicationDate)
                            .query();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return resultList;
    }

    long getLastPublicationDate() {
        return lastPublicationDate;
    }

    void loadNext(long lastPublicationDate) {
        this.lastPublicationDate = lastPublicationDate;

        onContentChanged();
    }
}
