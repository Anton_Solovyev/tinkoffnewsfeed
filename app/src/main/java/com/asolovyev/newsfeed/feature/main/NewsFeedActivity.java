package com.asolovyev.newsfeed.feature.main;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.asolovyev.newsfeed.R;
import com.paginate.Paginate;

public class NewsFeedActivity extends AppCompatActivity implements Contract.View,
        NewsFeedAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    //================================================================================
    // Fields
    //================================================================================

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private NewsFeedAdapter adapter;
    private TextView errorView;

    private NewsFeedPresenter presenter;

    //================================================================================
    // AppCompatActivity methods override
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsfeed);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false));
        adapter = new NewsFeedAdapter(this, this);
        recyclerView.setAdapter(adapter);

        errorView = (TextView) findViewById(R.id.errorView);

        presenter = new NewsFeedPresenter();
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        presenter.detachView();
        super.onStop();
    }

    //================================================================================
    // Contract.View implementation
    //================================================================================

    @Override
    public Paginate attachPaginationCallbacks(Paginate.Callbacks callbacks) {
        return Paginate.with(recyclerView, callbacks)
                .addLoadingListItem(true)
                .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                .build();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onRefresh() {
        presenter.updateData();
    }

    @Override
    public void setDataSet(DataSet dataSet) {
        adapter.setDataSet(dataSet);
    }

    @Override
    public void setErrorVisible(boolean isVisible) {
        errorView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setRefreshing(boolean isRefreshing) {
        swipeRefreshLayout.setRefreshing(isRefreshing);
    }

    //================================================================================
    // NewsFeedAdapter.OnItemClickListener implementation
    //================================================================================

    @Override
    public void onItemClicked(int itemPosition) {
        presenter.showContentForNewsFeedAtPos(itemPosition);
    }
}
