package com.asolovyev.newsfeed.feature.main;

import android.support.v4.app.LoaderManager;

import com.asolovyev.newsfeed.shared.mvp.BasePresenter;
import com.asolovyev.newsfeed.shared.mvp.BaseView;
import com.paginate.Paginate;

class Contract {
    interface Presenter extends BasePresenter<View>, Paginate.Callbacks {
        void showContentForNewsFeedAtPos(int position);
        void updateData();
    }

    public interface View extends BaseView<Presenter> {
        Paginate attachPaginationCallbacks(Paginate.Callbacks callbacks);
        LoaderManager getSupportLoaderManager();
        void setDataSet(DataSet dataSet);
        void setErrorVisible(boolean isVisible);
        void setRefreshing(boolean isLoading);
    }
}
