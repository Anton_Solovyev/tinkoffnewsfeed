package com.asolovyev.newsfeed.feature.content;

import android.os.Bundle;
import android.text.TextUtils;

import com.asolovyev.newsfeed.model.NewsFeed;
import com.asolovyev.newsfeed.shared.utils.DateFormatter;
import com.asolovyev.newsfeed.shared.utils.HtmlTextUtils;
import com.asolovyev.newsfeed.sync.SyncService;
import com.asolovyev.newsfeed.sync.event.NewsFeedContentSyncStatusEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

class NewsFeedContentPresenter implements Contract.Presenter{
    //================================================================================
    // Fields
    //================================================================================

    private Contract.View view;
    private NewsFeed newsFeed;

    NewsFeedContentPresenter() {
    }

    //================================================================================
    // Contract.View implementation
    //================================================================================

    @Override
    public void attachView(Contract.View view) {
        this.view = view;

        EventBus.getDefault().register(this);

        final Bundle extras = view.getExtras();
        if(extras == null) {
            return;
        }

        this.newsFeed = extras.getParcelable(Contract.ARG_NEWS_FEED);
        if(newsFeed == null) {
            return;
        }

        this.view.setDate(DateFormatter.formatDateTimestamp(newsFeed.getPublicationDate()));
        this.view.setTitle(HtmlTextUtils.fromHtml(newsFeed.getText()));
        this.view.setContentText(HtmlTextUtils.fromHtml(newsFeed.getContent()));

        SyncService.syncNewsFeedContent(view.getContext(), this.newsFeed.getServerID());
    }

    @Override
    public void detachView() {
        EventBus.getDefault().unregister(this);
        this.view = null;
    }

    //================================================================================
    // EventBus receiver
    //================================================================================

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSyncEventStatusReceived(NewsFeedContentSyncStatusEvent event) {
        if(!TextUtils.equals(this.newsFeed.getServerID(), event.getServerID())) {
            return;
        }

        switch (event.getStatus()) {
            case BEGIN:
                view.setErrorVisible(false);
                view.setProgressBarVisible(true);
                break;

            case COMPLETED:
                view.setErrorVisible(false);
                view.setProgressBarVisible(false);
                view.setContentText(HtmlTextUtils.fromHtml(event.getContent()));
                break;

            case FAILED:
                if(newsFeed.getContent() == null) {
                    view.setErrorVisible(true);
                }
                view.setProgressBarVisible(false);
                break;
        }
    }
}
