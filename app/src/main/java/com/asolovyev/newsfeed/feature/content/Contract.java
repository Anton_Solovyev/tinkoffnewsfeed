package com.asolovyev.newsfeed.feature.content;

import android.os.Bundle;
import android.text.Spanned;

import com.asolovyev.newsfeed.shared.mvp.BasePresenter;
import com.asolovyev.newsfeed.shared.mvp.BaseView;

class Contract {
    static final String ARG_NEWS_FEED = "news_feed";

    interface Presenter extends BasePresenter<View> {
    }

    interface View extends BaseView<Presenter> {
        Bundle getExtras();

        void setContentText(Spanned contentText);

        void setDate(String date);

        void setErrorVisible(boolean isVisible);

        void setProgressBarVisible(boolean isVisible);

        void setTitle(Spanned title);
    }
}
