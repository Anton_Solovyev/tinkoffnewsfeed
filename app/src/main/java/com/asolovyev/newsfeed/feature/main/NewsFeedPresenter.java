package com.asolovyev.newsfeed.feature.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.asolovyev.newsfeed.NewsFeedApp;
import com.asolovyev.newsfeed.db.Database;
import com.asolovyev.newsfeed.feature.content.NewsFeedContentActivity;
import com.asolovyev.newsfeed.model.NewsFeed;
import com.asolovyev.newsfeed.sync.SyncService;
import com.asolovyev.newsfeed.sync.event.NewsFeedSyncStatusEvent;
import com.paginate.Paginate;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

public class NewsFeedPresenter implements Contract.Presenter,
        LoaderManager.LoaderCallbacks<List<NewsFeed>> {

    //================================================================================
    // Fields
    //================================================================================
    private final int NEWS_FEED_LOADER_ID = 1;

    @Inject
    Database database;
    private Contract.View view;
    private final DataSet dataSet = new DataSet();
    private Paginate paginate;

    NewsFeedPresenter() {
        NewsFeedApp.getAppComponent().inject(this);
    }

    //================================================================================
    // Contract.Presenter implementation
    //================================================================================

    @Override
    public void attachView(Contract.View view) {
        this.view = view;

        EventBus.getDefault().register(this);

        if(paginate == null) {
            paginate = view.attachPaginationCallbacks(this);
        }

        view.setDataSet(dataSet);

        if(view.getSupportLoaderManager().getLoader(NEWS_FEED_LOADER_ID) == null) {
            view.getSupportLoaderManager().initLoader(NEWS_FEED_LOADER_ID, null, this);
        }
    }

    @Override
    public void detachView() {
        EventBus.getDefault().unregister(this);

        this.view = null;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return dataSet.isHasLoadedAllItems();
    }

    @Override
    public boolean isLoading() {
        final Loader loader = view.getSupportLoaderManager().getLoader(NEWS_FEED_LOADER_ID);
        if(loader != null) {
            final NewsFeedLoader nfl = (NewsFeedLoader) loader;
            return nfl.getLastPublicationDate() == dataSet.getLastPublicationDate();
        }

        return false;
    }

    @Override
    public void onLoadMore() {
        final Loader loader = view.getSupportLoaderManager().getLoader(NEWS_FEED_LOADER_ID);
        if(loader != null) {
            final NewsFeedLoader nfl = (NewsFeedLoader) loader;
            nfl.loadNext(dataSet.getLastPublicationDate());
        }
    }

    @Override
    public void showContentForNewsFeedAtPos(int position) {
        final Intent intent = NewsFeedContentActivity.createIntent(view.getContext(),
                dataSet.getItems().get(position));
        view.getContext().startActivity(intent);
    }

    @Override
    public void updateData() {
        SyncService.syncNewsFeed(view.getContext());
    }

    //================================================================================
    // EventBus receiver
    //================================================================================

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSyncEventStatusReceived(NewsFeedSyncStatusEvent event) {
        switch (event.getStatus()) {
            case BEGIN:
                view.setErrorVisible(false);
                view.setRefreshing(true);
                break;

            case COMPLETED:
                view.setErrorVisible(false);
                view.setRefreshing(false);
                view.getSupportLoaderManager().restartLoader(NEWS_FEED_LOADER_ID, null, this);
                break;

            case FAILED:
                view.setRefreshing(false);
                if(dataSet.getItems().isEmpty()) {
                    view.setErrorVisible(true);
                    view.getSupportLoaderManager().destroyLoader(NEWS_FEED_LOADER_ID);
                }
                break;
        }
    }

    //================================================================================
    // LoaderManager.LoaderCallbacks implementation
    //================================================================================

    @Override
    public Loader<List<NewsFeed>> onCreateLoader(int id, Bundle args) {
        return new NewsFeedLoader(view.getContext(), database);
    }

    @Override
    public void onLoadFinished(Loader<List<NewsFeed>> loader, List<NewsFeed> data) {
        final int loadedItemsQuantity = data.size();
        dataSet.setHasLoadedAllItems(loadedItemsQuantity < Config.ITEMS_PER_PAGE);

        if(loadedItemsQuantity > 0) {
            dataSet.setLastPublicationDate(data.get(loadedItemsQuantity - 1).getPublicationDate());
        }

        dataSet.addItems(data);

        // load data when data set is empty
        if(dataSet.getItems().isEmpty()) {
            updateData();
        }
    }

    @Override
    public void onLoaderReset(Loader<List<NewsFeed>> loader) {
    }
}
