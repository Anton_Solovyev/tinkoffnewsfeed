package com.asolovyev.newsfeed.feature.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asolovyev.newsfeed.R;
import com.paginate.recycler.LoadingListItemCreator;

class CustomLoadingListItemCreator implements LoadingListItemCreator {
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.item_loading_in_progress, parent, false);
        return new LoadingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    }

    private static class LoadingViewHolder extends RecyclerView.ViewHolder{
        LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }
}