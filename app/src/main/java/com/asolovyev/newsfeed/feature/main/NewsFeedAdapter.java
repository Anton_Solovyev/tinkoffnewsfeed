package com.asolovyev.newsfeed.feature.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.asolovyev.newsfeed.R;
import com.asolovyev.newsfeed.model.NewsFeed;
import com.asolovyev.newsfeed.shared.utils.DateFormatter;
import com.asolovyev.newsfeed.shared.utils.HtmlTextUtils;

class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.ViewHolder> {
    //================================================================================
    // Fields
    //================================================================================

    private final Context context;
    private DataSet dataSet;
    private OnItemClickListener itemClickListener;

    NewsFeedAdapter(@NonNull Context context, @NonNull OnItemClickListener itemClickListener) {
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    //================================================================================
    // RecyclerView.Adapter methods override
    //================================================================================

    @Override
    public NewsFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_newsfeed,
                parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final NewsFeed nf = dataSet.getItems().get(position);
        holder.dateView.setText(DateFormatter.formatDateTimestamp(nf.getPublicationDate()));
        holder.textView.setText(HtmlTextUtils.fromHtml(nf.getText()));

        holder.itemView.setOnClickListener(holder);
    }

    @Override
    public boolean onFailedToRecycleView(ViewHolder holder) {
        holder.itemView.setOnClickListener(null);
        return true;
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.getItems().size();
    }

    void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
        dataSet.attachAdapter(this);

        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        final TextView dateView;
        final TextView textView;

        ViewHolder(View itemView) {
            super(itemView);

            dateView = (TextView) itemView.findViewById(R.id.dateView);
            textView = (TextView) itemView.findViewById(R.id.textView);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION) {
                itemClickListener.onItemClicked(position);
            }
        }
    }

    interface OnItemClickListener {
        void onItemClicked(int itemPosition);
    }
}
