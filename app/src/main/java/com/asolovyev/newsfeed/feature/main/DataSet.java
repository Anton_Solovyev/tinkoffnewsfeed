package com.asolovyev.newsfeed.feature.main;

import android.support.v7.widget.RecyclerView;

import com.asolovyev.newsfeed.model.NewsFeed;

import java.util.ArrayList;
import java.util.List;

class DataSet {
    private RecyclerView.Adapter adapter;
    private List<NewsFeed> newsFeedList = new ArrayList<>();

    private long lastPublicationDate;
    private boolean hasLoadedAllItems = false;

    void addItems(List<NewsFeed> items) {
        this.newsFeedList.addAll(items);

        this.adapter.notifyDataSetChanged();
    }

    void attachAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    List<NewsFeed> getItems() {
        return newsFeedList;
    }

    long getLastPublicationDate() {
        return lastPublicationDate;
    }

    boolean isHasLoadedAllItems() {
        return hasLoadedAllItems;
    }

    void setHasLoadedAllItems(boolean hasLoadedAllItems) {
        this.hasLoadedAllItems = hasLoadedAllItems;
    }

    void setLastPublicationDate(long lastPublicationDate) {
        this.lastPublicationDate = lastPublicationDate;
    }
}
