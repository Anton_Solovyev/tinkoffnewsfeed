package com.asolovyev.newsfeed;

import android.app.Application;

import com.asolovyev.newsfeed.dagger.component.AppComponent;
import com.asolovyev.newsfeed.dagger.component.DaggerAppComponent;
import com.asolovyev.newsfeed.dagger.module.AppModule;
import com.asolovyev.newsfeed.dagger.module.DatabaseModule;
import com.asolovyev.newsfeed.dagger.module.NetworkClientModule;

public class NewsFeedApp extends Application {
    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .databaseModule(new DatabaseModule())
                .networkClientModule(new NetworkClientModule())
                .build();
    }
}
