package com.asolovyev.newsfeed.dagger.module;


import com.asolovyev.newsfeed.network.NetworkClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkClientModule {
    public NetworkClientModule() {
    }

    @Singleton
    @Provides
    NetworkClient providesNetworkClient() {
        return new NetworkClient();
    }
}
