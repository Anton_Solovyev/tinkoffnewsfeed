package com.asolovyev.newsfeed.dagger.module;

import android.app.Application;

import com.asolovyev.newsfeed.db.Database;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {
    public DatabaseModule() {
    }

    @Singleton
    @Provides
    Database providesDatabase(Application application) {
        return new Database(application);
    }
}
