package com.asolovyev.newsfeed.dagger.component;

import com.asolovyev.newsfeed.dagger.module.AppModule;
import com.asolovyev.newsfeed.dagger.module.DatabaseModule;
import com.asolovyev.newsfeed.dagger.module.NetworkClientModule;
import com.asolovyev.newsfeed.feature.main.NewsFeedPresenter;
import com.asolovyev.newsfeed.sync.SyncService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, DatabaseModule.class, NetworkClientModule.class})
public interface AppComponent {
    void inject(NewsFeedPresenter presenter);

    void inject(SyncService service);
}
