package com.asolovyev.newsfeed.sync.event;

import com.asolovyev.newsfeed.sync.Status;

public class NewsFeedSyncStatusEvent extends BaseSyncStatusEvent {
    public NewsFeedSyncStatusEvent(Status status) {
        super(status);
    }
}
