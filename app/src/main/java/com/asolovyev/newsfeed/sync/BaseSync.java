package com.asolovyev.newsfeed.sync;

import android.content.Context;
import android.support.annotation.NonNull;

import com.asolovyev.newsfeed.db.Database;
import com.asolovyev.newsfeed.network.NetworkClient;

abstract class BaseSync {
    private final Context context;
    private final Database database;
    private final NetworkClient client;

    BaseSync(@NonNull Context context, @NonNull Database database, @NonNull NetworkClient client) {
        this.context = context.getApplicationContext();
        this.database = database;
        this.client = client;
    }

    public void startSync() {
    }

    protected Context getContext() {
        return context;
    }

    protected Database getDatabase() {
        return database;
    }

    protected NetworkClient getClient() { return client; }
}
