package com.asolovyev.newsfeed.sync.event;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.asolovyev.newsfeed.sync.Status;

public class NewsFeedContentSyncStatusEvent extends BaseSyncStatusEvent {
    private String serverID;
    private String content;

    public NewsFeedContentSyncStatusEvent(@NonNull Status status,
                                          @NonNull String serverID,
                                          @Nullable String content) {
        super(status);

        this.serverID = serverID;
        this.content = content;
    }

    public String getServerID() {
        return serverID;
    }

    public String getContent() {
        return content;
    }
}
