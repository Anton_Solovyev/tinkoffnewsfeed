package com.asolovyev.newsfeed.sync;

enum Type {
    NEWS_FEED,
    NEWS_FEED_CONTENT
}
