package com.asolovyev.newsfeed.sync;

public enum Status {
    BEGIN,
    COMPLETED,
    FAILED
}
