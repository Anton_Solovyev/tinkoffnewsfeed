package com.asolovyev.newsfeed.sync.event;

import com.asolovyev.newsfeed.sync.Status;

public abstract class BaseSyncStatusEvent {
    private Status status;

    BaseSyncStatusEvent(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }
}
