package com.asolovyev.newsfeed.sync;

import android.content.Context;
import android.support.annotation.NonNull;

import com.asolovyev.newsfeed.db.Database;
import com.asolovyev.newsfeed.db.TablesConfig;
import com.asolovyev.newsfeed.model.NewsFeed;
import com.asolovyev.newsfeed.network.response.NewsFeedResponse;
import com.asolovyev.newsfeed.network.NetworkClient;
import com.asolovyev.newsfeed.sync.event.NewsFeedSyncStatusEvent;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

class NewsFeedSync extends BaseSync {
    NewsFeedSync(@NonNull Context context,
                 @NonNull Database db,
                 @NonNull NetworkClient client) {
        super(context, db, client);
    }

    @Override
    public void startSync() {
        sendStatusEvent(Status.BEGIN);

        final List<NewsFeed> newsFeedList = getNewsFeedList();
        if(newsFeedList == null) {
            sendStatusEvent(Status.FAILED);
            return;
        }

        saveNewsFeedList(newsFeedList);
        sendStatusEvent(Status.COMPLETED);
    }

    private Dao<NewsFeed, Long> getNewsFeedDao() {
        Dao<NewsFeed, Long> newsFeedDao = null;
        try {
            newsFeedDao = getDatabase().getNewsFeedDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newsFeedDao;
    }

    private List<NewsFeed> getNewsFeedList() {
        final Call<NewsFeedResponse> call = getClient().getApi().getNews();

        Response<NewsFeedResponse> executedResponse = null;
        try {
            executedResponse = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!isSuccessResponse(executedResponse)) {
            return null;
        }

        return executedResponse.body().getPayload();
    }

    private boolean isNewsFeedExist(Dao<NewsFeed, Long> dao, String serverID) {
        boolean isExist = false;
        try {
            isExist = dao.queryForEq(TablesConfig.NewsFeed.SERVER_ID, serverID).size() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return isExist;
    }

    private boolean isSuccessResponse(Response<NewsFeedResponse> executedResponse) {
        //noinspection SimplifiableIfStatement
        if(executedResponse == null || !executedResponse.isSuccessful()) {
            return false;
        }

        return "ok".equalsIgnoreCase(executedResponse.body().getResultCode());
    }

    private void saveNewsFeedList(List<NewsFeed> newsFeedList) {
        final Dao<NewsFeed, Long> newsFeedDao = getNewsFeedDao();
        if(newsFeedDao == null) {
            return;
        }

        for(NewsFeed nf : newsFeedList) {
            try {
                if(isNewsFeedExist(newsFeedDao, nf.getServerID())) {
                    final UpdateBuilder<NewsFeed, Long> builder = newsFeedDao.updateBuilder();
                    builder
                            .updateColumnValue(TablesConfig.NewsFeed.NAME, new SelectArg(nf.getName()))
                            .updateColumnValue(TablesConfig.NewsFeed.TEXT, new SelectArg(nf.getText()))
                            .updateColumnValue(TablesConfig.NewsFeed.PUBLICATION_DATE, nf.getPublicationDate())
                            .updateColumnValue(TablesConfig.NewsFeed.BANK_INFO_TYPE_ID, nf.getBankInfoTypeId());
                    builder.where().eq(TablesConfig.NewsFeed.SERVER_ID, nf.getServerID());
                    builder.update();
                } else {
                    newsFeedDao.createOrUpdate(nf);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendStatusEvent(Status status) {
        EventBus.getDefault().post(new NewsFeedSyncStatusEvent(status));
    }
}
