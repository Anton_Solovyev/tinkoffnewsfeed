package com.asolovyev.newsfeed.sync;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.asolovyev.newsfeed.db.Database;
import com.asolovyev.newsfeed.db.TablesConfig;
import com.asolovyev.newsfeed.model.NewsFeed;
import com.asolovyev.newsfeed.network.NetworkClient;
import com.asolovyev.newsfeed.network.response.NewsFeedContentResponse;
import com.asolovyev.newsfeed.sync.event.NewsFeedContentSyncStatusEvent;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.sql.SQLException;

import retrofit2.Call;
import retrofit2.Response;

class NewsFeedContentSync extends BaseSync{
    private final String serverID;

    NewsFeedContentSync(@NonNull Context context,
                        @NonNull Database db,
                        @NonNull NetworkClient client,
                        @NonNull String serverID) {
        super(context, db, client);

        this.serverID = serverID;
    }

    @Override
    public void startSync() {
        sendStatusEvent(Status.BEGIN, null);

        final String content = getContent();
        if(content == null) {
            sendStatusEvent(Status.FAILED, null);
            return;
        }

        saveContent(content);
        sendStatusEvent(Status.COMPLETED, content);
    }

    private void sendStatusEvent(@NonNull Status status,
                                 @Nullable String content) {
        EventBus.getDefault().post(new NewsFeedContentSyncStatusEvent(status, serverID, content));
    }

    private String getContent() {
        final Call<NewsFeedContentResponse> call = getClient().getApi().getNewsContent(serverID);

        Response<NewsFeedContentResponse> executedResponse = null;
        try {
            executedResponse = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!isSuccessResponse(executedResponse)) {
            return null;
        }

        return executedResponse.body().getPayload().getContent();
    }

    private void saveContent(String content) {
        final Dao<NewsFeed, Long> newsFeedDao = getNewsFeedDao();
        if(newsFeedDao == null) {
            return;
        }

        final UpdateBuilder<NewsFeed, Long> builder = newsFeedDao.updateBuilder();
        try {
            builder.updateColumnValue(TablesConfig.NewsFeed.CONTENT, new SelectArg(content));
            builder.where().eq(TablesConfig.NewsFeed.SERVER_ID, serverID);
            builder.update();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    private Dao<NewsFeed, Long> getNewsFeedDao() {
        Dao<NewsFeed, Long> newsFeedDao = null;
        try {
            newsFeedDao = getDatabase().getNewsFeedDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newsFeedDao;
    }

    private boolean isSuccessResponse(Response<NewsFeedContentResponse> executedResponse) {
        if(executedResponse == null || !executedResponse.isSuccessful()) {
            return false;
        }

        //noinspection SimplifiableIfStatement
        if(!"ok".equalsIgnoreCase(executedResponse.body().getResultCode())) {
            return false;
        }

        return executedResponse.body().getPayload() != null;
    }
}
