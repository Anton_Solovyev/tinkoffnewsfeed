package com.asolovyev.newsfeed.sync;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.asolovyev.newsfeed.NewsFeedApp;
import com.asolovyev.newsfeed.db.Database;
import com.asolovyev.newsfeed.network.NetworkClient;

import javax.inject.Inject;

public class SyncService extends IntentService{
    private static final String ARG_REQUEST_TYPE = "request_type";
    private static final String ARG_EXTRA = "extra";

    //================================================================================
    // Start sync requests
    //================================================================================

    public static void syncNewsFeed(@NonNull Context context) {
        final Intent intent = new Intent(context, SyncService.class);
        intent.putExtra(ARG_REQUEST_TYPE, Type.NEWS_FEED);
        context.startService(intent);
    }

    public static void syncNewsFeedContent(@NonNull Context context, @NonNull String serverID) {
        final Intent intent = new Intent(context, SyncService.class);
        intent.putExtra(ARG_REQUEST_TYPE, Type.NEWS_FEED_CONTENT);
        intent.putExtra(ARG_EXTRA, serverID);
        context.startService(intent);
    }

    //================================================================================
    // Fields
    //================================================================================

    @Inject
    Database database;

    @Inject
    NetworkClient client;

    //================================================================================
    // SyncService implementation
    //================================================================================

    public SyncService() {
        super("SyncService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        NewsFeedApp.getAppComponent().inject(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(intent == null) {
            return;
        }

        final Type type = (Type) intent.getSerializableExtra(ARG_REQUEST_TYPE);
        switch (type) {
            case NEWS_FEED:
                new NewsFeedSync(this, database, client).startSync();
                break;

            case NEWS_FEED_CONTENT:
                final String serverID = intent.getStringExtra(ARG_EXTRA);
                new NewsFeedContentSync(this, database, client, serverID).startSync();
                break;
        }
    }
}
