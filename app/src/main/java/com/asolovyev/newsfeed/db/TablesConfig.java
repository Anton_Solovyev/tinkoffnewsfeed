package com.asolovyev.newsfeed.db;

public class TablesConfig {
    public static final String ID = "_id";

    public static class NewsFeed {
        public static final String SERVER_ID = "server_id";
        public static final String NAME = "name";
        public static final String TEXT = "text";
        public static final String PUBLICATION_DATE = "publicationDate";
        public static final String BANK_INFO_TYPE_ID = "bank_info_type_id";
        public static final String CONTENT = "content";
    }
}
