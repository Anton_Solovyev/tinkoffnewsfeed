package com.asolovyev.newsfeed.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.asolovyev.newsfeed.model.NewsFeed;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;

public class Database extends OrmLiteSqliteOpenHelper{
    private static final String DB_NAME = "news_feed.db";
    private static final int DB_VERSION = 1;

    private volatile Dao<NewsFeed, Long> newsFeedDao;

    public Database(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, NewsFeed.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, NewsFeed.class, false);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        onCreate(database);
    }

    public Dao<NewsFeed, Long> getNewsFeedDao() throws SQLException {
        if (newsFeedDao == null) {
            synchronized (this) {
                if (newsFeedDao == null) {
                    newsFeedDao = getDao(NewsFeed.class);
                }
            }
        }

        return newsFeedDao;
    }
}
