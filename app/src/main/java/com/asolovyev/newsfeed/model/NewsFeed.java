package com.asolovyev.newsfeed.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.asolovyev.newsfeed.db.TablesConfig;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class NewsFeed implements Parcelable{
    @DatabaseField(columnName = TablesConfig.ID, generatedId = true)
    private long id;

    @DatabaseField(columnName = TablesConfig.NewsFeed.SERVER_ID, canBeNull = false, uniqueIndex = true)
    private String serverID;

    @DatabaseField(columnName = TablesConfig.NewsFeed.NAME, canBeNull = false)
    private String name;

    @DatabaseField(columnName = TablesConfig.NewsFeed.TEXT, canBeNull = false)
    private String text;

    @DatabaseField(columnName = TablesConfig.NewsFeed.PUBLICATION_DATE, canBeNull = false)
    private long publicationDate;

    @DatabaseField(columnName = TablesConfig.NewsFeed.BANK_INFO_TYPE_ID, canBeNull = false)
    private int bankInfoTypeId;

    @DatabaseField(columnName = TablesConfig.NewsFeed.CONTENT)
    private String content;

    public NewsFeed() {
    }

    public NewsFeed(String serverID,
                    String name,
                    String text,
                    long publicationDate,
                    int bankInfoTypeId) {
        this.serverID = serverID;
        this.name = name;
        this.text = text;
        this.publicationDate = publicationDate;
        this.bankInfoTypeId = bankInfoTypeId;
    }

    public long getID() {
        return id;
    }

    public String getServerID() {
        return serverID;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public long getPublicationDate() {
        return publicationDate;
    }

    public int getBankInfoTypeId() {
        return bankInfoTypeId;
    }

    public String getContent() {
        return content;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.serverID);
        dest.writeString(this.name);
        dest.writeString(this.text);
        dest.writeLong(this.publicationDate);
        dest.writeInt(this.bankInfoTypeId);
        dest.writeString(this.content);
    }

    protected NewsFeed(Parcel in) {
        this.id = in.readLong();
        this.serverID = in.readString();
        this.name = in.readString();
        this.text = in.readString();
        this.publicationDate = in.readLong();
        this.bankInfoTypeId = in.readInt();
        this.content = in.readString();
    }

    public static final Creator<NewsFeed> CREATOR = new Creator<NewsFeed>() {
        @Override
        public NewsFeed createFromParcel(Parcel source) {
            return new NewsFeed(source);
        }

        @Override
        public NewsFeed[] newArray(int size) {
            return new NewsFeed[size];
        }
    };
}
