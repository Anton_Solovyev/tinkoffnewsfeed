package com.asolovyev.newsfeed.shared.utils;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

public class HtmlTextUtils {
    public static Spanned fromHtml(String htmlStr) {
        if(htmlStr == null) {
            return null;
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(htmlStr,Html.FROM_HTML_MODE_LEGACY);
        } else {
            //noinspection deprecation
            return Html.fromHtml(htmlStr);
        }
    }
}
