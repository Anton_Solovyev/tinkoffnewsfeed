package com.asolovyev.newsfeed.shared.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

@SuppressWarnings("WeakerAccess")
public abstract class BaseLoader<T> extends AsyncTaskLoader<T> {
    protected T cache;

    public BaseLoader(Context context) {
        super(context);
    }

    @Override
    public void deliverResult(T data) {
        if (isReset()) {
            if (data != null) {
                onReleaseResources(data);
            }
        }

        T oldCache = cache;
        cache = data;

        if (isStarted()) {
            if(data != null) {
                super.deliverResult(data);
            }
        }

        if (oldCache != null) {
            onReleaseResources(oldCache);
        }
    }

    /**
     * Handles a request to start the Loader.
     */
    @Override
    protected void onStartLoading() {
        if (cache != null) {
            deliverResult(cache);
        }

        registerReceivers();

        if (takeContentChanged() || cache == null) {
            // If the data has changed since the last time it was loaded
            // or is not currently available, start a load.
            forceLoad();
        }
    }


    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    public void onCanceled(T data) {
        super.onCanceled(data);

        onReleaseResources(data);
    }

    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();

        if (cache != null) {
            onReleaseResources(cache);
            cache = null;
        }

        unregisterReceivers();
    }

    @SuppressWarnings("UnusedParameters")
    protected void onReleaseResources(T data) {
    }

    protected void registerReceivers() {
    }

    protected void unregisterReceivers() {

    }
}
