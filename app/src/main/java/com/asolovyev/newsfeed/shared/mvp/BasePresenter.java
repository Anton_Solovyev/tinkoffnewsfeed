package com.asolovyev.newsfeed.shared.mvp;

public interface BasePresenter<T> {
    void attachView(T view);
    void detachView();
}
