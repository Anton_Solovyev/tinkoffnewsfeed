package com.asolovyev.newsfeed.shared.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy, kk:mm",
            Locale.getDefault());

    public static String formatDateTimestamp(long timestamp) {
        return dateFormat.format(new Date(timestamp));
    }
}
