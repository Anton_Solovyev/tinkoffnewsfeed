package com.asolovyev.newsfeed.shared.mvp;

import android.content.Context;

@SuppressWarnings("unused")
public interface BaseView<T> {
    Context getContext();
}
